$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#scrollup').fadeIn();
        } else {
            $('#scrollup').fadeOut();
        }
    });

    $('#scrollup').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

});

function slowScroll(id){
	var offset = 60;
	$('html, body').animate({
		scrollTop: $(id).offset().top - offset
	}, 500);
	return false;
}
function toggle_menu(){
$('header ul').toggle();
}

$( window ).resize(function() {
    if ($( window ).width() > 768){
        $('header ul').show();
    }
});